﻿namespace UrlShortener.Models
{
    public class Url
    {
        public string UrlLong { get; set; }
        public string UrlShort { get; set; }
        public Url()
        {
        }
        public Url(string urlLong, string urlShort)
        {
            UrlLong = urlLong;
            UrlShort = urlShort;
        }
    }
}
