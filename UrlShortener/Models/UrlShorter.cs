﻿

using Microsoft.AspNetCore.Authentication;
using Newtonsoft.Json;
using System.Text;

namespace UrlShortener.Models
{
    public class UrlShorter
    {
        private const string BitlyToken = "a402b144babc3aa6c2947e666f9c6e616959b44b";


        private readonly IHttpClientFactory _httpClientFactory;

        public UrlShorter(IHttpClientFactory httpClientFactory)
        {                
            _httpClientFactory = httpClientFactory;
        }
        public async Task<string> ShortenThisUrl(string url)
        {
            if(!IsValidUrlNameLength(url) && !ContainProtocol(url))
            {
                return "No valida";
            }
            string shortedUrl = await ShortenUrlWithBitly(url);
            return shortedUrl;

        }

        public bool IsValidUrlNameLength(string url)
        {
            return url.Length >= 14;
        }

        public bool ContainProtocol(string url)
        {
            return url.IndexOf("https", StringComparison.OrdinalIgnoreCase) >= 0 ||
                url.IndexOf("http", StringComparison.OrdinalIgnoreCase) >= 0;
        }

        private async Task<string> ShortenUrlWithBitly(string url)
        {
           string apiUrl = "https://api-ssl.bitly.com/v4/shorten";
           
            var client = _httpClientFactory.CreateClient("BackEnd");
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", BitlyToken);
            string requestJson = $"{{\"long_url\": \"{url}\"}}";
            var content = new StringContent(requestJson, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(apiUrl, content);
            if (response.IsSuccessStatusCode)
            {
                var jsonResponse = await response.Content.ReadAsStringAsync();
                dynamic dynamicResponse = JsonConvert.DeserializeObject(jsonResponse);
                return dynamicResponse.link;
            }
            throw new HttpRequestException(response.RequestMessage.ToString());
            
        }
       
    }
}
