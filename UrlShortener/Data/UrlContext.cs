﻿using Microsoft.EntityFrameworkCore;

namespace UrlShortener.Data
{
    public class UrlContext : DbContext
    {
        public UrlContext(DbContextOptions<UrlContext>options) : base(options) 
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UrlEntity>().HasKey(u => new { u.UrlLong, u.UrlShort });
        }
        public DbSet<UrlEntity> Urls { get; set; }
    }
    public class UrlEntity
    {
        public string UrlLong { get; set; }
        public string UrlShort { get; set; }
    }
}
