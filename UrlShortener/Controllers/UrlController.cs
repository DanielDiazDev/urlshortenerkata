﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UrlShortener.Data;
using UrlShortener.Models;
using UrlShortener.Models.ModelViews;
using UrlShortener.Services;

namespace UrlShortener.Controllers
{
    public class UrlController : Controller
    {
        private IDatabaseService databaseService;
        private UrlShorter urlShorter;
        public UrlController(IDatabaseService databaseService, IHttpClientFactory httpClientFactory) 
        {
            this.databaseService = databaseService;
            urlShorter = new UrlShorter(httpClientFactory);

        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(UrlViewModel model)
        {
            if (ModelState.IsValid)
            {               
                string urlShort = await urlShorter.ShortenThisUrl(model.UrlLong);
                Url url = new Url(model.UrlLong, urlShort);
                await databaseService.Add(url);
                Url createdUrl = await databaseService.Get(model.UrlLong);
                return RedirectToAction(nameof(UrlCreatedSuccess), createdUrl);
            }
            return View(model);
        }
        public IActionResult UrlCreatedSuccess(Url createdUrl)
        {
            
            return View(createdUrl);
        }
        public IActionResult Test()
        {
            return View();
        }
    }
}
