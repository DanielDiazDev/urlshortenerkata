﻿using Microsoft.EntityFrameworkCore;
using UrlShortener.Data;
using UrlShortener.Models;

namespace UrlShortener.Services
{
    public interface IDatabaseService
    {
        Task<bool> Add(Url url);
        //Task<List<Url>> GetAll();
        Task<Url> Get(string urlLong);
    }
    public class DatabaseService : IDatabaseService
    {
        UrlContext context;

        public DatabaseService(UrlContext context)
        {
            this.context = context;
        }
        public async Task<bool> Add(Url url)
        {
            UrlEntity urlEntity = new UrlEntity();
            urlEntity.UrlLong = url.UrlLong;
            urlEntity.UrlShort = url.UrlShort;
            await context.AddAsync(urlEntity);
            await context.SaveChangesAsync();
            return true;
        }

        public async Task<Url> Get(string urlLong)
        {
            UrlEntity urlEntity = await context.Urls.FirstOrDefaultAsync(u => u.UrlLong == urlLong);
            if (urlEntity != null)
            {
                Url url = new Url(urlEntity.UrlLong, urlEntity.UrlShort);
                return url;
            }
            return null;
        }
    }
}
